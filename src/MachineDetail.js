import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBJumbotron, MDBBtn } from "mdbreact";
import {
	getMachineDetail,
	updateMachineDetail
} from "./store/actions/getMachinesList.act";
import { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Progress from "react-progressbar";
import { healthColor } from "./common/changeHealtColor";
import { Formik } from "formik";
import * as Yup from "yup";
let getId = null;
const MachineDetail = props => {
	useEffect(() => {
		let getPathname = window.location.pathname;
		if (getPathname !== undefined) {
			let pathNameArr = getPathname.split("/");
			getId = pathNameArr[2];
			props.dispatch(getMachineDetail(getId));
		}
	}, []);
	return (
		<MDBContainer>
			{props.detail !== null ? (
				<MDBRow>
					<MDBCol>
						<h1>{props.detail.name}</h1>
						<h4>Update Device</h4>
						<Formik
							initialValues={{ name: "" }}
							validationSchema={Yup.object({
								name: Yup.string().required("Required")
							})}
							onSubmit={(values, actions) => {
								let payload = JSON.stringify(values);
								props.dispatch(
									updateMachineDetail(getId, payload)
								);
								actions.setSubmitting(false);
								actions.resetForm();
							}}
						>
							{props => (
								<form onSubmit={props.handleSubmit}>
									{(props.errors.name ||
										props.errors.required) && (
										<div
											id="feedback"
											className="text-danger"
										>
											{props.errors.name ||
												props.errors.required}
										</div>
									)}
									<input
										type="text"
										name="name"
										className="form-control"
										placeholder="Enter the name"
										onChange={props.handleChange}
										onBlur={props.handleBlur}
										value={props.values.name}
									/>
									<MDBBtn color="primary" type="submit">
										Submit
									</MDBBtn>
								</form>
							)}
						</Formik>
					</MDBCol>
					<MDBCol>
						<MDBJumbotron fluid className="text-center">
							<MDBContainer>
								<h2 className="display-4">
									{props.detail.health}
								</h2>
								<p className="lead">
									<Progress
										completed={props.detail.health}
										color={healthColor(props.detail.health)}
									/>
								</p>
							</MDBContainer>
						</MDBJumbotron>
						<h4>Stats</h4>
						IP Address : {props.detail.ip_address}
					</MDBCol>
				</MDBRow>
			) : (
				"Loading..."
			)}
		</MDBContainer>
	);
};
MachineDetail.propTypes = {
	getMachinesList: PropTypes.checkPropTypes(),
	dispatch: PropTypes.checkPropTypes(),
	detail: PropTypes.checkPropTypes(),
	handleSubmit: PropTypes.checkPropTypes(),
	handleChange: PropTypes.checkPropTypes(),
	handleBlur: PropTypes.checkPropTypes(),
	values: PropTypes.checkPropTypes(),
	errors: PropTypes.checkPropTypes()
};

const mapStateToProps = state => {
	return {
		loading: state.Machine.loading,
		detail: state.Machine.detail
	};
};
export default connect(mapStateToProps, null)(MachineDetail);

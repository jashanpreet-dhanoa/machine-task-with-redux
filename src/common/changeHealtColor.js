export const healthColor = percentage => {
	if (percentage <= 50) {
		return "#5cb85c";
	} else if (percentage > 50 && percentage >= 70) {
		return "#f0ad4e";
	} else if (percentage > 70 && percentage >= 100) {
		return "#f0ad4e";
	}
};

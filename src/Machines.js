import React, { useEffect } from "react";
import { connect } from "react-redux";
import {
	getMachinesList,
	getMachinesListByWs
} from "./store/actions/getMachinesList.act";
import PropTypes from "prop-types";
import Progress from "react-progressbar";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import { push } from "react-router-redirect";
import { healthColor } from "./common/changeHealtColor";
import { w3cwebsocket as W3CWebSocket } from "websocket";
const client = new W3CWebSocket("ws://localhost:1337");

const Machines = props => {
	useEffect(() => {
		props.dispatch(getMachinesList());

		client.onopen = () => {
			console.log("WebSocket Client Connected");
		};
		client.onmessage = message => {
			props.dispatch(getMachinesListByWs(message));
		};
	}, []);
	const handleClick = (e, id) => {
		push("/machines/" + id + "/");
	};

	return (
		<React.Fragment>
			<MDBTable>
				<MDBTableHead>
					<tr>
						<th>Name</th>
						<th>Ip Address</th>
						<th>Health</th>
					</tr>
				</MDBTableHead>
				<MDBTableBody>
					{!props.error && props.getList !== null
						? props.getList.map((data, index) => {
								return (
									<tr
										key={index}
										onClick={e => handleClick(e, data.id)}
										className="cursor-pointer"
									>
										<td key={index}>{data.name}</td>
										<td key={index}>{data.ip_address}</td>
										<td key={index}>
											<Progress
												completed={data.health}
												color={healthColor(data.health)}
											/>
										</td>
									</tr>
								);
						  })
						: "loading"}
				</MDBTableBody>
			</MDBTable>
		</React.Fragment>
	);
};

Machines.propTypes = {
	getMachinesList: PropTypes.checkPropTypes(),
	dispatch: PropTypes.checkPropTypes(),
	getList: PropTypes.checkPropTypes(),
	error: PropTypes.checkPropTypes()
};
const mapStateToProps = state => {
	return {
		loading: state.Machine.loading,
		getList: state.Machine.list,
		error: state.Machine.error
	};
};
export default connect(mapStateToProps, null)(Machines);

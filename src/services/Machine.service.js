import axios from "axios";
import getOptions from "./header";
export const MachineServices = {
	getMachinesList: getMachinesList,
	getMachineDetail: getMachineDetail,
	updateMachineDetail: updateMachineDetail
};
function getMachinesList() {
	return axios
		.get("http://localhost:8080/machines", getOptions())
		.then(response => {
			return response;
		})
		.catch(err => {
			return err;
		});
}

function getMachineDetail(id) {
	return axios
		.get("http://localhost:8080/machines/" + id, getOptions())
		.then(response => {
			return response;
		})
		.catch(err => {
			return err;
		});
}

function updateMachineDetail(id, payload) {
	return axios
		.put("http://localhost:8080/machines/" + id, payload, getOptions())
		.then(response => {
			return response;
		})
		.catch(err => {
			return err;
		});
}

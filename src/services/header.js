function getOptions() {
	let options = {};
	if (localStorage.getItem("token")) {
		options.headers = {
			Authorization: "bearer " + localStorage.getItem("token"),
			"Content-Type": "application/json",
			Accept: "application/json"
			// 'Access-Control-Allow-Origin': '*'
		};
	} else {
		options.headers = {
			"Content-Type": "application/json",
			Accept: "application/json"
			// 'Access-Control-Allow-Origin': '*'
		};
	}

	return options;
}

export default getOptions;

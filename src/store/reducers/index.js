import { combineReducers } from "redux";
import { Machine } from "./machine.red";
const rootReducer = combineReducers({
	Machine: Machine
});

export default rootReducer;

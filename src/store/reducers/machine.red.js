import * as actionTypes from "../actions/types/machine_list.type";
import { updateObject } from "../utility";
const initialState = {
	error: false,
	loading: false,
	list: null,
	detail: null
};
/**
 * get machines list
 */
export const getListStart = (state, action) => {
	return updateObject(state, {
		loading: true,
		error: false
	});
};

export const getListSuccess = (state, action) => {
	return updateObject(state, {
		loading: false,
		list: action.data,
		error: false
	});
};

export const getListFail = (state, action) => {
	return updateObject(state, {
		loading: false,
		error: true,
		list: null
	});
};
/**
 * Get individual machine detail
 */
export const getDetailStart = (state, action) => {
	return updateObject(state, {
		loading: true,
		error: false
	});
};

export const getDetailSuccess = (state, action) => {
	return updateObject(state, {
		loading: false,
		detail: action.data,
		error: false
	});
};

export const getDetailFail = (state, action) => {
	return updateObject(state, {
		loading: false,
		error: true
	});
};
/**
 * Update Machine details
 */
export const updateDetailStart = (state, action) => {
	return updateObject(state, {
		loading: true,
		error: false
	});
};

export const updateDetailSuccess = (state, action) => {
	return updateObject(state, {
		loading: false,
		detail: action.data,
		error: false
	});
};

export const updateDetailFail = (state, action) => {
	return updateObject(state, {
		loading: false,
		error: false
	});
};

/**
 * update health by ws
 */
export const updateHealthByWs = (state, action) => {
	let getList = [...state.list];
	let updatedList = JSON.parse(action.data);
	let getIndex = getList.findIndex(item => item.id === updatedList.id);
	getList[getIndex].health = updatedList.health;

	return updateObject(state, {
		list: getList
	});
};

export const Machine = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GET_MACHINE_LIST_START:
			return getListStart(state, action);
		case actionTypes.GET_MACHINE_LIST_SUCCESS:
			return getListSuccess(state, action);
		case actionTypes.GET_MACHINE_LIST_FAIL:
			return getListFail(state, action);

		case actionTypes.GET_MACHINE_DETAIL_START:
			return getDetailStart(state, action);
		case actionTypes.GET_MACHINE_DETAIL_SUCCESS:
			return getDetailSuccess(state, action);
		case actionTypes.GET_MACHINE_DETAIL_FAIL:
			return getDetailFail(state, action);

		case actionTypes.UPDATE_MACHINE_DETAIL_START:
			return updateDetailStart(state, action);
		case actionTypes.UPDATE_MACHINE_DETAIL_SUCCESS:
			return updateDetailSuccess(state, action);
		case actionTypes.UPDATE_MACHINE_DETAIL_FAIL:
			return updateDetailFail(state, action);
		case actionTypes.UPDATE_HEALTH_BY_WS:
			return updateHealthByWs(state, action);

		default:
			return state;
	}
};

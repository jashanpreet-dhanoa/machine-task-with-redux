import {
	GET_MACHINE_LIST_FAIL,
	GET_MACHINE_LIST_START,
	GET_MACHINE_LIST_SUCCESS,
	GET_MACHINE_DETAIL_START,
	GET_MACHINE_DETAIL_SUCCESS,
	GET_MACHINE_DETAIL_FAIL,
	UPDATE_HEALTH_BY_WS
} from "../actions/types/machine_list.type";
import { MachineServices } from "../../services/Machine.service";
export const getMachinesListStart = () => {
	return {
		type: GET_MACHINE_LIST_START
	};
};

/**
 * GET COMPANY LIST
 */
export const getMachinesListSuccess = data => {
	return {
		type: GET_MACHINE_LIST_SUCCESS,
		data: data
	};
};

export const getMachinesListFail = () => {
	return {
		type: GET_MACHINE_LIST_FAIL
	};
};

export const getMachinesList = () => {
	return dispatch => {
		dispatch(getMachinesListStart());
		MachineServices.getMachinesList()
			.then(res => {
				dispatch(getMachinesListSuccess(res.data));
			})
			.catch(err => {
				dispatch(getMachinesListFail());
			});
	};
};

/**
 * GET SIGNLE COMPANY DETAIL
 */

export const getMachineDetailStart = () => {
	return {
		type: GET_MACHINE_DETAIL_START
	};
};

export const getMachineDetailSuccess = data => {
	return {
		type: GET_MACHINE_DETAIL_SUCCESS,
		data: data
	};
};

export const getMachineDetailFail = () => {
	return {
		type: GET_MACHINE_DETAIL_FAIL
	};
};

export const getMachineDetail = id => {
	return dispatch => {
		dispatch(getMachineDetailStart());
		MachineServices.getMachineDetail(id)
			.then(res => {
				dispatch(getMachineDetailSuccess(res.data));
			})
			.catch(err => {
				dispatch(getMachineDetailFail());
			});
	};
};

/**
 * UPDATE COMPANY DETAIL
 */

export const updateMachineDetailStart = () => {
	return {
		type: GET_MACHINE_DETAIL_START
	};
};

export const updateMachineDetailSuccess = data => {
	return {
		type: GET_MACHINE_DETAIL_SUCCESS,
		data: data
	};
};

export const updateMachineDetailFail = () => {
	return {
		type: GET_MACHINE_DETAIL_FAIL
	};
};

export const updateMachineDetail = (id, payload) => {
	return dispatch => {
		dispatch(updateMachineDetailStart());
		MachineServices.updateMachineDetail(id, payload)
			.then(res => {
				dispatch(updateMachineDetailSuccess(res.data));
			})
			.catch(err => {
				dispatch(updateMachineDetailFail());
			});
	};
};

/**
 * web socket for live listing
 */
export const getMachinesListByWsSuccess = data => {
	return {
		type: UPDATE_HEALTH_BY_WS,
		data: data
	};
};

export const getMachinesListByWs = data => {
	return dispatch => {
		dispatch(getMachinesListByWsSuccess(data.data));
	};
};
